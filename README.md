Main branch

1. Creamos el dockerfile que contiene la configuración de las imagenes de php, apache y mysql
y lo metemos en la carpeta phpApache

2. Creamos un docker compose que contenga dos servicios: apache + php y la base de datos mysql, ademas de 
su propia network

3. Metemos en la carpeta project nuestro proyecto

4. Modificamos el archivo dbconnection para incluir nuestro contenedor mysql

4. lanzamos el comando docker compose up -d
